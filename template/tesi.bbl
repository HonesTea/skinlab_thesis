\begin{thebibliography}{10}

\bibitem{badler1982modelling}
Norman~I Badler and Mary~Ann Morris.
\newblock Modelling flexible articulated objects.
\newblock In {\em Proc. Computer Graphics' 82, Online Conf}, pages 305--314,
  1982.

\bibitem{baran2007automatic}
Ilya Baran and Jovan Popovi{\'c}.
\newblock Automatic rigging and animation of 3d characters.
\newblock In {\em ACM Transactions on Graphics (TOG)}, volume~26, page~72. ACM,
  2007.

\bibitem{SkeletonLab}
Simone Barbieri, Pietro Meloni, Francesco Usai, and Riccardo Scateni.
\newblock Skeleton lab: an interactive tool to create, edit, and repair
  curve-skeletons.
\newblock 2015.

\bibitem{sarafabri}
Sara Casti and Fabrizio Corda.
\newblock Cagelab: Interactive tool for cage-based animation.
\newblock Master's thesis, Università degli studi di Cagliari, 2015.

\bibitem{clifford1882classification}
WK~Clifford.
\newblock On the classification of geometric algebras, published as paper xliii
  in mathematical papers.(tucker, r., ed), 1882.

\bibitem{pinocchioModel}
Dassault Systèmes~SolidWorks Corp.
\newblock Cosmic blobs® models.
\newblock \url{http://www.mit.edu/~ibaran/autorig/pinocchio.html}.
\newblock Accessed: 2016-08-25.

\bibitem{jacobson2011bounded}
Alec Jacobson, Ilya Baran, Jovan Popovic, and Olga Sorkine.
\newblock Bounded biharmonic weights for real-time deformation.
\newblock {\em ACM Trans. Graph.}, 30(4):78, 2011.

\bibitem{jacobson2014skinning}
Alec Jacobson, Zhigang Deng, Ladislav Kavan, and J~Lewis.
\newblock Skinning: real-time shape deformation.
\newblock In {\em ACM SIGGRAPH}, 2014.

\bibitem{jacobson2010mixed}
Alec Jacobson, Elif Tosun, Olga Sorkine, and Denis Zorin.
\newblock Mixed finite elements for variational surface modeling.
\newblock In {\em Computer graphics forum}, volume~29, pages 1565--1574. Wiley
  Online Library, 2010.

\bibitem{jacobson2012smooth}
Alec Jacobson, Tino Weinkauf, and Olga Sorkine.
\newblock Smooth shape-aware functions with controlled extrema.
\newblock In {\em Computer Graphics Forum}, volume~31, pages 1577--1586. Wiley
  Online Library, 2012.

\bibitem{kavan2008geometric}
Ladislav Kavan, Steven Collins, Ji{\v{r}}{\'\i} {\v{Z}}{\'a}ra, and Carol
  O'Sullivan.
\newblock Geometric skinning with approximate dual quaternion blending.
\newblock {\em ACM Transactions on Graphics (TOG)}, 27(4):105, 2008.

\bibitem{kenwright2012beginners}
Ben Kenwright.
\newblock A beginners guide to dual-quaternions.
\newblock {\em WSCG'2012}, 2012.

\bibitem{livesu3d}
Marco Livesu.
\newblock 3d-from-2d curve-skeleton extraction.
\newblock \url{https://sites.google.com/site/marcolivesu/curve-skeletons}.
\newblock Accessed: 2016-08-25.

\bibitem{luciano2000avatar}
Cristian Luciano and Pat Banerjee.
\newblock Avatar kinematics modeling for telecollaborative virtual
  environments.
\newblock In {\em Proceedings of the 32nd conference on Winter simulation},
  pages 1533--1538. Society for Computer Simulation International, 2000.

\bibitem{magnenat1988joint}
Nadia Magnenat-Thalmann, Richard Laperrire, and Daniel Thalmann.
\newblock Joint-dependent local deformations for hand animation and object
  grasping.
\newblock In {\em In Proceedings on Graphics interface’88}. Citeseer, 1988.

\bibitem{meyer2001discrete}
Mark Meyer, Mathieu Desbrun, Peter Schr{\"o}der, and Alan~H Barr.
\newblock Discrete differential-geometry operators for triangulated
  2-manifolds, 2001.

\bibitem{mukundan2002quaternions}
R~Mukundan.
\newblock Quaternions: From classical mechanics to computer graphics, and
  beyond.
\newblock In {\em Proceedings of the 7th Asian Technology conference in
  Mathematics}, pages 97--105, 2002.

\bibitem{parent2012computer}
Rick Parent.
\newblock {\em Computer animation: algorithms and techniques}.
\newblock Newnes, 2012.

\bibitem{perez2004dual}
Alba Perez and J~Michael McCarthy.
\newblock Dual quaternion synthesis of constrained robotic systems.
\newblock {\em Journal of Mechanical Design}, 126(3):425--435, 2004.

\bibitem{sayas2008gentle}
Francisco-Javier Sayas.
\newblock A gentle introduction to the finite element method.
\newblock {\em Cited on}, page~40, 2008.

\bibitem{scateni2005fondamenti}
Riccardo Scateni.
\newblock {\em Fondamenti di grafica tridimensionale interattiva}.
\newblock McGraw-Hill, 2005.

\bibitem{shoemake1985animating}
Ken Shoemake.
\newblock Animating rotation with quaternion curves.
\newblock In {\em ACM SIGGRAPH computer graphics}, volume~19, pages 245--254.
  ACM, 1985.

\bibitem{wardetzky2007discrete}
Max Wardetzky, Mikl{\'o}s Bergou, David Harmon, Denis Zorin, and Eitan
  Grinspun.
\newblock Discrete quadratic curvature energies.
\newblock {\em Computer Aided Geometric Design}, 24(8):499--518, 2007.

\end{thebibliography}
