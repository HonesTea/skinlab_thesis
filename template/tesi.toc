\select@language {american}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Background}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Computer Animation}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Skeletal Animation}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Skinning}{7}{section.2.3}
\contentsline {section}{\numberline {2.4}Quaternions}{8}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Quaternions arithmetic}{9}{subsection.2.4.1}
\contentsline {section}{\numberline {2.5}Dual-quaternions}{9}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Dual-quaternions arithmetic}{9}{subsection.2.5.1}
\contentsline {section}{\numberline {2.6}Finite Element Methods}{11}{section.2.6}
\contentsline {section}{\numberline {2.7}Used technologies}{13}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Qt}{13}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}C++}{13}{subsection.2.7.2}
\contentsline {subsection}{\numberline {2.7.3}OpenGL}{13}{subsection.2.7.3}
\contentsline {subsection}{\numberline {2.7.4}LibQGLViewer}{13}{subsection.2.7.4}
\contentsline {subsection}{\numberline {2.7.5}Eigen}{14}{subsection.2.7.5}
\contentsline {subsection}{\numberline {2.7.6}Gurobi}{14}{subsection.2.7.6}
\contentsline {subsection}{\numberline {2.7.7}TetGen}{14}{subsection.2.7.7}
\contentsline {chapter}{\numberline {3}Skinning weights}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Rigid weights}{15}{section.3.1}
\contentsline {section}{\numberline {3.2}Bone-heat diffusion weights}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}Biharmonic weights}{17}{section.3.3}
\contentsline {chapter}{\numberline {4}Skinning algorithms}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Linear Blend Skinning}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}Dual Quaternion Skinning}{20}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Algorithms Comparison}{22}{subsection.4.2.1}
\contentsline {chapter}{\numberline {5}SkinLab}{25}{chapter.5}
\contentsline {section}{\numberline {5.1}Weights implementation}{26}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Rigid Weights}{27}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}BoneHeat Weights}{28}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Bounded Biharmonic Weights}{28}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Dual Quaternion Skinning}{30}{section.5.2}
\contentsline {section}{\numberline {5.3}User Interface}{31}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Actions widget}{32}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Weights Widget}{33}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Save Widget}{34}{subsection.5.3.3}
\contentsline {chapter}{\numberline {6}Results}{37}{chapter.6}
\contentsline {chapter}{\numberline {7}Future works}{45}{chapter.7}
\contentsline {paragraph}{Skeleton}{45}{section*.35}
\contentsline {paragraph}{Conical form of the quadratic problem}{45}{section*.36}
\contentsline {paragraph}{Scaling}{45}{section*.37}
\contentsline {paragraph}{Easier manipulation}{45}{section*.38}
\contentsline {paragraph}{Multiple interaction device}{46}{section*.39}
\contentsline {chapter}{\numberline {8}Conclusions}{47}{chapter.8}
\contentsline {chapter}{References}{50}{appendix*.40}
